//Require nativo de Node js
//const fs = require('fs');
//No es nativo de Node js
//const fs = require('express');
//Creados por nosotros
//const fs = require('./fs');

//Destructuracion
const { crearArchivo, leerArchivo } = require('./multiplicar/multiplicar');
//
const argv = require('./config/yargs').argv;

const colors = require('colors');

let comando = argv._[0];

switch (comando) {
    case 'listar':
        leerArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`La tabla del ${argv.base} es;\n${archivo}`))
            .catch(e => {
                console.log(e);
            });
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado: ${archivo}`.green))
            .catch(e => {
                console.log(e);
            });
        break;
    default:
        console.log('Comando no reconocido');
        break;
}