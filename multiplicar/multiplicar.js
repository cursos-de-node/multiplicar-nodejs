const fs = require('fs');
const colors = require('colors');

//Exportar funcion
//module.exports.crearArchivo = (base) => {
let crearArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject(`El valor introducido ${base} no es un numero`);
            return;
        }

        let data = '';

        for (let i = 1; i <= limite; i++) {
            data += `${base} x ${i} = ${base * i} \n`;
        }

        fs.writeFile(`./tablas/tabla-${base}.txt`, data, (err) => {
            if (err)
                reject(err);
            else
                resolve(`tabla-${base}.txt`);
        });
    });
};

let leerArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {

        console.log('------------'.green);

        let data = '';

        for (let i = 1; i <= limite; i++) {
            data += `${base} x ${i} = ${base * i} \n`;
        }

        resolve(data.toString());
    });
};

//Exportar funcion
module.exports = {
    crearArchivo,
    leerArchivo
};